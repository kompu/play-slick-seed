# --- !Ups

-- object: public."user" | type: TABLE --
-- DROP TABLE IF EXISTS public."user" CASCADE;
CREATE TABLE public."user"(
	id serial NOT NULL,
	provider_id text NOT NULL,
	provider_key text NOT NULL,
	user_uuid text NOT NULL,
	first_name text,
	last_name text,
	full_name text,
	email text,
	avatar_url text,
	CONSTRAINT user_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public."user" OWNER TO postgres;
-- ddl-end --

-- object: public.password_info | type: TABLE --
-- DROP TABLE IF EXISTS public.password_info CASCADE;
CREATE TABLE public.password_info(
	id serial NOT NULL,
	hasher text NOT NULL,
	password text NOT NULL,
	salt text,
	reset_token text,
	user_id integer NOT NULL,
	CONSTRAINT password_info_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.password_info OWNER TO postgres;
-- ddl-end --

-- object: public.oauth2_info | type: TABLE --
-- DROP TABLE IF EXISTS public.oauth2_info CASCADE;
CREATE TABLE public.oauth2_info(
	id serial NOT NULL,
	access_token text NOT NULL,
	token_type text,
	expires_in integer,
	refresh_token text,
	user_id integer NOT NULL,
	CONSTRAINT oauth2_info_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.oauth2_info OWNER TO postgres;
-- ddl-end --

-- object: password_info_user_id_fk | type: CONSTRAINT --
-- ALTER TABLE public.password_info DROP CONSTRAINT IF EXISTS password_info_user_id_fk CASCADE;
ALTER TABLE public.password_info ADD CONSTRAINT password_info_user_id_fk FOREIGN KEY (user_id)
REFERENCES public."user" (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: oauth2_info_user_id_fk | type: CONSTRAINT --
-- ALTER TABLE public.oauth2_info DROP CONSTRAINT IF EXISTS oauth2_info_user_id_fk CASCADE;
ALTER TABLE public.oauth2_info ADD CONSTRAINT oauth2_info_user_id_fk FOREIGN KEY (user_id)
REFERENCES public."user" (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

# --- !Downs