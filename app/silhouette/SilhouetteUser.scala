package silhouette

import java.util.UUID

import com.mohiva.play.silhouette.api.{Identity, LoginInfo}

case class SilhouetteUser(
  userID: UUID,
  loginInfo: LoginInfo,
  firstName: Option[String],
  lastName: Option[String],
  fullName: Option[String],
  email: Option[String],
  avatarURL: Option[String]) extends Identity
