package dao

import java.util.UUID

import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.LoginInfo
import models.Tables
import models.UserRow
import play.api.db.slick.DatabaseConfigProvider
import silhouette.SilhouetteUser
import play.api.libs.concurrent.Execution.Implicits._
import slick.driver.JdbcProfile

import scala.concurrent.Future

@Singleton
class UserDao @Inject()(val dbConfigProvider: DatabaseConfigProvider) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]
  val db = dbConfig.db
  import Tables.profile.api._

  def all : Future[Seq[UserRow]] = {
    db.run(Tables.User.result)
  }

  def insertUser(user: UserRow) : Future[UserRow] = {
    db.run((Tables.User returning Tables.User) += user)
  }

  // Silhouette
  def find(loginInfo: LoginInfo) : Future[Option[SilhouetteUser]] = {
    val q = Tables.User.filter(u => u.providerId === loginInfo.providerID && u.providerKey === loginInfo.providerKey)
    db.run(q.result.headOption).map(userOption =>
      userOption.map(user =>
        SilhouetteUser(
          UUID.fromString(user.userUuid),
          LoginInfo(user.providerId, user.providerKey),
          user.firstName,
          user.lastName,
          user.fullName,
          user.email,
          user.avatarUrl)
      )
    )
  }

  def find(userID: UUID) : Future[Option[SilhouetteUser]] = {
    val q = Tables.User.filter(u => u.userUuid === userID.toString)
    db.run(q.result.headOption).map(userOption =>
      userOption.map(user =>
        SilhouetteUser(
          UUID.fromString(user.userUuid),
          LoginInfo(user.providerId, user.providerKey),
          user.firstName,
          user.lastName,
          user.fullName,
          user.email,
          user.avatarUrl)
      )
    )
  }

  def save(user: SilhouetteUser) = {
    val q = Tables.User.filter(u => u.userUuid === user.userID.toString)
    val actions = (for {
      userOption <- q.result.headOption
      _ <- Tables.User.insertOrUpdate(userOption.getOrElse(UserRow(
        0,
        user.loginInfo.providerID,
        user.loginInfo.providerKey,
        user.userID.toString,
        user.firstName,
        user.lastName,
        user.fullName,
        user.email,
        user.avatarURL
      )))
    } yield()).transactionally
    db.run(actions).map(_ => user)
  }
}