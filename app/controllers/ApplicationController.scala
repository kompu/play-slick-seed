package controllers

import com.google.inject.Inject
import com.mohiva.play.silhouette.api.{LogoutEvent, Silhouette, Environment}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import com.mohiva.play.silhouette.impl.providers.SocialProviderRegistry
import forms.{SignUpForm, SignInForm}
import play.api.i18n.MessagesApi
import silhouette.SilhouetteUser

import scala.concurrent.Future

class ApplicationController @Inject()(
  val messagesApi: MessagesApi,
  val env: Environment[SilhouetteUser, CookieAuthenticator],
  socialProviderRegistry: SocialProviderRegistry) extends Silhouette[SilhouetteUser, CookieAuthenticator] {

  /**
    * Handles the index action.
    *
    * @return The result to display.
    */
  def index = SecuredAction { implicit request =>
    Ok(views.html.home(request.identity))
  }

  /**
    * Handles the Sign In action.
    *
    * @return The result to display.
    */
  def signIn = UserAwareAction { implicit request =>
    request.identity match {
      case Some(user) => Redirect(routes.ApplicationController.index())
      case None => Ok(views.html.signIn(SignInForm.form, socialProviderRegistry))
    }
  }

  /**
    * Handles the Sign Up action.
    *
    * @return The result to display.
    */
  def signUp = UserAwareAction { implicit request =>
    request.identity match {
      case Some(user) => Redirect(routes.ApplicationController.index())
      case None => Ok(views.html.signUp(SignUpForm.form))
    }
  }

  /**
    * Handles the Sign Out action.
    *
    * @return The result to display.
    */
  def signOut = SecuredAction.async { implicit request =>
    val result = Redirect(routes.ApplicationController.index())
    env.eventBus.publish(LogoutEvent(request.identity, request, request2Messages))

    env.authenticatorService.discard(request.authenticator, result)
  }
}
