package controllers

import com.google.inject.Inject
import com.mohiva.play.silhouette.api.{Silhouette, Environment}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import dao.UserDao
import models.UserRow
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{Messages, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc.{RequestHeader, Action}
import play.api.libs.concurrent.Execution.Implicits._
import silhouette.SilhouetteUser

import scala.concurrent.Future

class Users @Inject()(
  val userDao: UserDao,
  val messagesApi: MessagesApi,
  val env: Environment[SilhouetteUser, CookieAuthenticator]) extends Silhouette[SilhouetteUser, CookieAuthenticator] {

  override def onNotAuthenticated(request: RequestHeader) = {
    Some(Future.successful(Unauthorized(Json.obj("error" -> Messages("error.profileUnauth")))))
  }

  def index = Action {
    Ok("ok")
  }

  implicit val userFormat = Json.format[UserRow]

  case class UserData (username: String, password: String)

  val userForm = Form {
    mapping(
      "username" -> text,
      "password" -> text
    )(UserData.apply)(UserData.unapply)
  }

  def addUser = Action.async { implicit request =>
    val userData = userForm.bindFromRequest.get
    val user = UserRow(0, "", "", "", Option(userData.username), Option(userData.password))
    val inserted = userDao.insertUser(user)
    inserted.map(
      u => Redirect(routes.Users.getUsers())
    )
  }

  def getUsers = SecuredAction.async {
    val users = userDao.all
    users.map(
      u => Ok(Json.toJson(u))
    )
  }
}
