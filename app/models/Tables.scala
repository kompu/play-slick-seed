package models
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.driver.PostgresDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.driver.JdbcProfile
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Oauth2Info.schema ++ PasswordInfo.schema ++ PlayEvolutions.schema ++ User.schema
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema


  /** GetResult implicit for fetching Oauth2InfoRow objects using plain SQL queries */
  implicit def GetResultOauth2InfoRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]], e3: GR[Option[Int]]): GR[Oauth2InfoRow] = GR{
    prs => import prs._
    Oauth2InfoRow.tupled((<<[Int], <<[String], <<?[String], <<?[Int], <<?[String], <<[Int]))
  }
  /** Table description of table oauth2_info. Objects of this class serve as prototypes for rows in queries. */
  class Oauth2Info(_tableTag: Tag) extends Table[Oauth2InfoRow](_tableTag, "oauth2_info") {
    def * = (id, accessToken, tokenType, expiresIn, refreshToken, userId) <> (Oauth2InfoRow.tupled, Oauth2InfoRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(accessToken), tokenType, expiresIn, refreshToken, Rep.Some(userId)).shaped.<>({r=>import r._; _1.map(_=> Oauth2InfoRow.tupled((_1.get, _2.get, _3, _4, _5, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column access_token SqlType(text) */
    val accessToken: Rep[String] = column[String]("access_token")
    /** Database column token_type SqlType(text), Default(None) */
    val tokenType: Rep[Option[String]] = column[Option[String]]("token_type", O.Default(None))
    /** Database column expires_in SqlType(int4), Default(None) */
    val expiresIn: Rep[Option[Int]] = column[Option[Int]]("expires_in", O.Default(None))
    /** Database column refresh_token SqlType(text), Default(None) */
    val refreshToken: Rep[Option[String]] = column[Option[String]]("refresh_token", O.Default(None))
    /** Database column user_id SqlType(int4) */
    val userId: Rep[Int] = column[Int]("user_id")

    /** Foreign key referencing User (database name oauth2_info_user_id_fk) */
    lazy val userFk = foreignKey("oauth2_info_user_id_fk", userId, User)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Oauth2Info */
  lazy val Oauth2Info = new TableQuery(tag => new Oauth2Info(tag))


  /** GetResult implicit for fetching PasswordInfoRow objects using plain SQL queries */
  implicit def GetResultPasswordInfoRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]]): GR[PasswordInfoRow] = GR{
    prs => import prs._
    PasswordInfoRow.tupled((<<[Int], <<[String], <<[String], <<?[String], <<?[String], <<[Int]))
  }
  /** Table description of table password_info. Objects of this class serve as prototypes for rows in queries. */
  class PasswordInfo(_tableTag: Tag) extends Table[PasswordInfoRow](_tableTag, "password_info") {
    def * = (id, hasher, password, salt, resetToken, userId) <> (PasswordInfoRow.tupled, PasswordInfoRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(hasher), Rep.Some(password), salt, resetToken, Rep.Some(userId)).shaped.<>({r=>import r._; _1.map(_=> PasswordInfoRow.tupled((_1.get, _2.get, _3.get, _4, _5, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column hasher SqlType(text) */
    val hasher: Rep[String] = column[String]("hasher")
    /** Database column password SqlType(text) */
    val password: Rep[String] = column[String]("password")
    /** Database column salt SqlType(text), Default(None) */
    val salt: Rep[Option[String]] = column[Option[String]]("salt", O.Default(None))
    /** Database column reset_token SqlType(text), Default(None) */
    val resetToken: Rep[Option[String]] = column[Option[String]]("reset_token", O.Default(None))
    /** Database column user_id SqlType(int4) */
    val userId: Rep[Int] = column[Int]("user_id")

    /** Foreign key referencing User (database name password_info_user_id_fk) */
    lazy val userFk = foreignKey("password_info_user_id_fk", userId, User)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table PasswordInfo */
  lazy val PasswordInfo = new TableQuery(tag => new PasswordInfo(tag))


  /** GetResult implicit for fetching PlayEvolutionsRow objects using plain SQL queries */
  implicit def GetResultPlayEvolutionsRow(implicit e0: GR[Int], e1: GR[String], e2: GR[java.sql.Timestamp], e3: GR[Option[String]]): GR[PlayEvolutionsRow] = GR{
    prs => import prs._
    PlayEvolutionsRow.tupled((<<[Int], <<[String], <<[java.sql.Timestamp], <<?[String], <<?[String], <<?[String], <<?[String]))
  }
  /** Table description of table play_evolutions. Objects of this class serve as prototypes for rows in queries. */
  class PlayEvolutions(_tableTag: Tag) extends Table[PlayEvolutionsRow](_tableTag, "play_evolutions") {
    def * = (id, hash, appliedAt, applyScript, revertScript, state, lastProblem) <> (PlayEvolutionsRow.tupled, PlayEvolutionsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(hash), Rep.Some(appliedAt), applyScript, revertScript, state, lastProblem).shaped.<>({r=>import r._; _1.map(_=> PlayEvolutionsRow.tupled((_1.get, _2.get, _3.get, _4, _5, _6, _7)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(int4), PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.PrimaryKey)
    /** Database column hash SqlType(varchar), Length(255,true) */
    val hash: Rep[String] = column[String]("hash", O.Length(255,varying=true))
    /** Database column applied_at SqlType(timestamp) */
    val appliedAt: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("applied_at")
    /** Database column apply_script SqlType(text), Default(None) */
    val applyScript: Rep[Option[String]] = column[Option[String]]("apply_script", O.Default(None))
    /** Database column revert_script SqlType(text), Default(None) */
    val revertScript: Rep[Option[String]] = column[Option[String]]("revert_script", O.Default(None))
    /** Database column state SqlType(varchar), Length(255,true), Default(None) */
    val state: Rep[Option[String]] = column[Option[String]]("state", O.Length(255,varying=true), O.Default(None))
    /** Database column last_problem SqlType(text), Default(None) */
    val lastProblem: Rep[Option[String]] = column[Option[String]]("last_problem", O.Default(None))
  }
  /** Collection-like TableQuery object for table PlayEvolutions */
  lazy val PlayEvolutions = new TableQuery(tag => new PlayEvolutions(tag))


  /** GetResult implicit for fetching UserRow objects using plain SQL queries */
  implicit def GetResultUserRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]]): GR[UserRow] = GR{
    prs => import prs._
    UserRow.tupled((<<[Int], <<[String], <<[String], <<[String], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String]))
  }
  /** Table description of table user. Objects of this class serve as prototypes for rows in queries. */
  class User(_tableTag: Tag) extends Table[UserRow](_tableTag, "user") {
    def * = (id, providerId, providerKey, userUuid, firstName, lastName, fullName, email, avatarUrl) <> (UserRow.tupled, UserRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(providerId), Rep.Some(providerKey), Rep.Some(userUuid), firstName, lastName, fullName, email, avatarUrl).shaped.<>({r=>import r._; _1.map(_=> UserRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6, _7, _8, _9)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column provider_id SqlType(text) */
    val providerId: Rep[String] = column[String]("provider_id")
    /** Database column provider_key SqlType(text) */
    val providerKey: Rep[String] = column[String]("provider_key")
    /** Database column user_uuid SqlType(text) */
    val userUuid: Rep[String] = column[String]("user_uuid")
    /** Database column first_name SqlType(text), Default(None) */
    val firstName: Rep[Option[String]] = column[Option[String]]("first_name", O.Default(None))
    /** Database column last_name SqlType(text), Default(None) */
    val lastName: Rep[Option[String]] = column[Option[String]]("last_name", O.Default(None))
    /** Database column full_name SqlType(text), Default(None) */
    val fullName: Rep[Option[String]] = column[Option[String]]("full_name", O.Default(None))
    /** Database column email SqlType(text), Default(None) */
    val email: Rep[Option[String]] = column[Option[String]]("email", O.Default(None))
    /** Database column avatar_url SqlType(text), Default(None) */
    val avatarUrl: Rep[Option[String]] = column[Option[String]]("avatar_url", O.Default(None))
  }
  /** Collection-like TableQuery object for table User */
  lazy val User = new TableQuery(tag => new User(tag))
}
/** Entity class storing rows of table Oauth2Info
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param accessToken Database column access_token SqlType(text)
   *  @param tokenType Database column token_type SqlType(text), Default(None)
   *  @param expiresIn Database column expires_in SqlType(int4), Default(None)
   *  @param refreshToken Database column refresh_token SqlType(text), Default(None)
   *  @param userId Database column user_id SqlType(int4) */
  case class Oauth2InfoRow(id: Int, accessToken: String, tokenType: Option[String] = None, expiresIn: Option[Int] = None, refreshToken: Option[String] = None, userId: Int)

  /** Entity class storing rows of table PasswordInfo
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param hasher Database column hasher SqlType(text)
   *  @param password Database column password SqlType(text)
   *  @param salt Database column salt SqlType(text), Default(None)
   *  @param resetToken Database column reset_token SqlType(text), Default(None)
   *  @param userId Database column user_id SqlType(int4) */
  case class PasswordInfoRow(id: Int, hasher: String, password: String, salt: Option[String] = None, resetToken: Option[String] = None, userId: Int)

  /** Entity class storing rows of table PlayEvolutions
   *  @param id Database column id SqlType(int4), PrimaryKey
   *  @param hash Database column hash SqlType(varchar), Length(255,true)
   *  @param appliedAt Database column applied_at SqlType(timestamp)
   *  @param applyScript Database column apply_script SqlType(text), Default(None)
   *  @param revertScript Database column revert_script SqlType(text), Default(None)
   *  @param state Database column state SqlType(varchar), Length(255,true), Default(None)
   *  @param lastProblem Database column last_problem SqlType(text), Default(None) */
  case class PlayEvolutionsRow(id: Int, hash: String, appliedAt: java.sql.Timestamp, applyScript: Option[String] = None, revertScript: Option[String] = None, state: Option[String] = None, lastProblem: Option[String] = None)

  /** Entity class storing rows of table User
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param providerId Database column provider_id SqlType(text)
   *  @param providerKey Database column provider_key SqlType(text)
   *  @param userUuid Database column user_uuid SqlType(text)
   *  @param firstName Database column first_name SqlType(text), Default(None)
   *  @param lastName Database column last_name SqlType(text), Default(None)
   *  @param fullName Database column full_name SqlType(text), Default(None)
   *  @param email Database column email SqlType(text), Default(None)
   *  @param avatarUrl Database column avatar_url SqlType(text), Default(None) */
  case class UserRow(id: Int, providerId: String, providerKey: String, userUuid: String, firstName: Option[String] = None, lastName: Option[String] = None, fullName: Option[String] = None, email: Option[String] = None, avatarUrl: Option[String] = None)
